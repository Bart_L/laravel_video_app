<?php
//routy zamkniete są w routcie z grupą middleware, aby mogly korzystać z metod w kernel.php - grupa web = aby można było korzystać np z globalnego zwracania błędów

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//navigation
Route::get('/contact', 'PagesController@contact');
Route::get('/about', 'PagesController@about');

/*videos - ważna jest kolejność scieżek, bo framework będzie je czytał z góry do dołu i w przypadku gdyby videos/id było wyżej,
to każde wwołanie czegokolwiek po slashu videos innego niż id, wywaliło by błąd*/
/*
Route::post('/videos', 'VideosController@store');
Route::get('/videos', 'VideosController@index');
Route::get('/videos/create', 'VideosController@create');
Route::get('/videos/{id}', 'VideosController@show');
*/
//aby skrócić powyższy zapis routów do videos i wrzucić je w jeden zasób, należy użyc resource, który sam przypisze funkcje do kontrolera i adresy po slashu
Route::resource('videos', 'VideosController');