{{--informacja jaki plik rozszerzany jest za pomocą tego pliku--}}
@extends('pages.master')
{{--informacja do jakiej sekcji w powyższym pliku trafia poniższy kontent--}}
@section('content')
<section class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
                <!-- Formularz -->

                @if(count($errors) >0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['url'=>'videos', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        <div class="col-md-4 control-label">
                            {!! Form::label('title', 'Tytuł:') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('title', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 control-label">
                            {!! Form::label('description', 'Opis:') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 control-label">
                            {!! Form::label('url', 'URL filmu:') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('url', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Dodaj film',['class'=>'btn btn-lg btn-primary']) !!}
                        </div>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
{{--oznaczenie końca sekcji--}}
@stop