{{--informacja jaki plik rozszerzany jest za pomocą tego pliku--}}
@extends('pages.master')
{{--informacja do jakiej sekcji w powyższym pliku trafia poniższy kontent--}}
@section('content')
    <div class="videos-header card">
        <h2>Najnowsze filmy</h2>
    </div>
    <div class="row">

        @foreach($videos as $video )
            <!-- Single video -->
            <div class="col-xs-12 col-md-6 col-lg-4 single-video">
                <div class="card">

                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $video->url }}?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="card-content">
                        <a href="{{ url('videos', $video->id) }}">
                            <h4>{{ $video->title }}</h4>
                        </a>
                        <p>{{ $video->description }}</p>
                        <span class="upper-label">Dodał</span>
                        <span class="video-author">Bartosz Ligęza</span>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
{{--oznaczenie końca sekcji--}}
@stop