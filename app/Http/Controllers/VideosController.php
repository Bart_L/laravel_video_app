<?php

namespace App\Http\Controllers;
//użycie requesta z laravel collective
use Request;

use App\Http\Requests;
use App\Http\Requests\CreateVideoRequest;
use App\Http\Controllers\Controller;
use App\Video;

class VideosController extends Controller
{
    //pobieranie listy filmów
    public function index() {
        //sortuje najpierw rekordy od najnowszych a potem pobiera i przypisuje do zmiennej videos
        $videos = Video::latest()->get();
        return view('videos.index')->with('videos', $videos);
    }

    //pobieranie pojedynczego filmu
    public function show($id) {
        //find or fail jeżeli nie znajdzie obiektu w klasie Video::  o podanym id - zwroci komunikat 404
        $video = Video::findOrFail($id);
        return view('videos.show')->with('video', $video);
    }

    //wyświetla formularz dodawania filmu
    public  function  create() {
        return view('videos.create');
    }

    //zapisuje film do bazy
    public  function  store(CreateVideoRequest $request)
    {
        Video::create($request->all());
        return redirect('videos');
    }

    //formularz edycji video
    public function edit($id) {
        //find or fail jeżeli nie znajdzie obiektu w klasie Video:: o podanym id - zwroci komunikat 404
        $video = Video::findOrFail($id);
        return view('videos.edit')->with('video', $video);
    }

    //aktualizacja filmu
    public function update($id, CreateVideoRequest $request) {
        //find or fail jeżeli nie znajdzie obiektu w klasie Video:: o podanym id - zwroci komunikat 404
        $video = Video::findOrFail($id);
        //po pobraniu id, kontroler updatatuje video wszystkimi requestami z inputów
        $video->update($request->all());
        return redirect('videos');
    }
}
