<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //$fillable opisuje jakie pola obiektu może dodawać użytkownik
    protected $fillable = [
        'title',
        'url',
        'description'
    ];
}
